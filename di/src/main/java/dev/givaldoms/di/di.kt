package dev.givaldoms.di

import dev.givaldoms.domain.featureA.GetSomeString
import dev.givaldoms.feature_a.FeatureANavigation
import dev.givaldoms.feature_b.FeatureBNavigation
import dev.givaldoms.intent.AppNavigation
import dev.givaldoms.presentation_a.FeatureA1ViewModel
import dev.givaldoms.presentation_a.FeatureA2ViewModel
import kotlinx.coroutines.CoroutineScope
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModules = module {

    single { AppNavigation() }

    single { AppNavigation() as FeatureANavigation }

    single { AppNavigation() as FeatureBNavigation }

    factory { (scope: CoroutineScope) ->
        GetSomeString(scope)
    }

    viewModel {
        FeatureA1ViewModel()
    }

    viewModel { FeatureA2ViewModel() }

}