package dev.givaldoms.base_app

import android.app.Application
import dev.givaldoms.di.appModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(appModules).androidContext(applicationContext)
        }
    }
}