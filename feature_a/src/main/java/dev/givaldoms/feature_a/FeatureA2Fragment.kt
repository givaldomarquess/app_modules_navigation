package dev.givaldoms.feature_a

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dev.givaldoms.BaseFragment
import dev.givaldoms.presentation_a.FeatureA2ViewModel
import kotlinx.android.synthetic.main.fragment_feature_a1.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class FeatureA2Fragment : BaseFragment() {

    private val viewModel: FeatureA2ViewModel by viewModel()
    private val navigation: FeatureANavigation by inject { parametersOf(this) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_feature_a2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(viewModel)

        container.setOnClickListener {
            navigation.goFromA2ToB1(it)
        }

        viewModel.getItems().onPostValue(this) {
            text.text = it
        }

    }

}
