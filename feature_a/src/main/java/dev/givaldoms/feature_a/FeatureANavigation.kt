package dev.givaldoms.feature_a

import android.view.View

interface FeatureANavigation {

    fun goFromA1ToA2(view: View)

    fun goFromA2ToB1(view: View)

}