package dev.givaldoms.intent

import android.view.View
import androidx.navigation.findNavController
import dev.givaldoms.feature_a.FeatureANavigation
import dev.givaldoms.feature_b.FeatureBNavigation

class AppNavigation : FeatureANavigation, FeatureBNavigation {

    override fun goFromA1ToA2(view: View) {
        view.findNavController().navigate(R.id.action_featureA1Fragment2_to_featureA2Fragment2)
    }

    override fun goFromB1ToB2(view: View) {
        view.findNavController().navigate(R.id.action_featureB1Fragment2_to_featureB2Fragment2)
    }

    override fun goFromA2ToB1(view: View) {
        view.findNavController().navigate(R.id.action_featureA2Fragment2_to_featureB1Fragment2)
    }

    override fun goFromB2ToA2(view: View) {
        view.findNavController().navigate(R.id.action_featureB2Fragment2_to_featureA2Fragment2)
    }
}