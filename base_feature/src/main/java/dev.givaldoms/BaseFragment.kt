package dev.givaldoms

import android.widget.Toast
import androidx.fragment.app.Fragment
import dev.givaldoms.base_presentation.ViewStateListener

abstract class BaseFragment : Fragment(), ViewStateListener {

    override fun onStateError(error: Throwable) {
        Toast.makeText(requireContext(), error.message, Toast.LENGTH_LONG).show()

    }

    override fun onStateLoading() {
        Toast.makeText(requireContext(), "Está carregando, tenha calma!", Toast.LENGTH_LONG).show()

    }
}
