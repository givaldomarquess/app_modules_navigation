package dev.givaldoms.presentation_a

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import dev.givaldoms.base_presentation.utils.asLiveData
import dev.givaldoms.base_presentation.utils.postError
import dev.givaldoms.base_presentation.utils.postLoading
import dev.givaldoms.base_presentation.utils.viewState

class FeatureA2ViewModel : ViewModel(), LifecycleObserver {

    private val items by viewState<String>()

    fun getItems() = items.asLiveData()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun fetchData() {
        items.postLoading()
        items.postError(RuntimeException("Houve um erro"))
    }
}
