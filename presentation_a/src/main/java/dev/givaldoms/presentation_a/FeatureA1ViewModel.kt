package dev.givaldoms.presentation_a

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import dev.givaldoms.base_presentation.utils.*
import dev.givaldoms.domain.featureA.GetSomeString
import org.koin.core.KoinComponent


open class FeatureA1ViewModel : ViewModel(), LifecycleObserver, KoinComponent {

    private val getSomeString: GetSomeString by useCase()
    private val items by viewState<String>()

    fun getItems() = items.asLiveData()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun fetchData() {
        items.postLoading()
        getSomeString.execute(
            params = Unit,
            onSuccess = { items.postSuccess(it) },
            onError = { items.postError(it) }
        )
    }


}
