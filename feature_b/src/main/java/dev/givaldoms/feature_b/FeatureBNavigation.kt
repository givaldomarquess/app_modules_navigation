package dev.givaldoms.feature_b

import android.view.View

interface FeatureBNavigation {

    fun goFromB1ToB2(view: View)

    fun goFromB2ToA2(view: View)

}