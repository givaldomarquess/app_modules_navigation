package dev.givaldoms.feature_b

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_feature_b2.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class FeatureB1Fragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_feature_b1, container, false)
    }

    private val navigation: FeatureBNavigation by inject { parametersOf(this) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        container.setOnClickListener {
            navigation.goFromB1ToB2(it)
        }
    }

}
