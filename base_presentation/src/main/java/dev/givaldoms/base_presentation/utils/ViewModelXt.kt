package dev.givaldoms.base_presentation.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dev.givaldoms.base_presentation.ViewState
import dev.givaldoms.domain.core.UseCase
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

fun <T> viewState() = lazy {
    EventLiveData<ViewState<T>>()
}

inline fun <V, reified U> V.useCase()
        where U : UseCase<*, *>, V : ViewModel, V : KoinComponent = inject<U> {
    parametersOf(viewModelScope)
}