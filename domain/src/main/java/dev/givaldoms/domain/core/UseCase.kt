package dev.givaldoms.domain.core

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

abstract class UseCase<out T : Any, in Params>(private val scope: CoroutineScope) {

    abstract suspend fun run(params: Params?): Either<Failure, T>

    fun execute(
        params: Params? = null,
        onError: (Failure) -> Unit,
        onSuccess: (T) -> Unit
    ) {
        scope.launch {
            run(params).either(
                { onError(it) },
                { onSuccess(it) }
            )
        }
    }

    fun cancel() = scope.cancel()


}