package dev.givaldoms.domain.core

sealed class Failure(override val message: String?) : Throwable() {
    object NetworkConnection : Failure("")
    object ServerError : Failure("")

    abstract class FeatureFailure : Failure("")
}