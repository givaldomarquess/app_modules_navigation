package dev.givaldoms.domain.featureA

import dev.givaldoms.domain.core.Either
import dev.givaldoms.domain.core.Failure
import dev.givaldoms.domain.core.UseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay

class GetSomeString(scope: CoroutineScope) : UseCase<String, Unit>(scope) {

    override suspend fun run(params: Unit?): Either<Failure, String> {
        delay(3000)
        return Either.Right("Teste")
    }
}